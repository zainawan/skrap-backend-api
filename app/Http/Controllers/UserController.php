<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function test(Request $request){
        $data = DB::raw("select * from tbl_users limit 1");
        return response()->json($data);
    }
}
