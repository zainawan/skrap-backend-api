<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeAllPrimaryColumnsToId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tables_meta = Schema::getAllTables();
        foreach($tables_meta as $index => $table_meta){
            $table = reset($table_meta);
            $primaryColumnInfo = DB::select(DB::raw("SHOW KEYS FROM `{$table}` WHERE Key_name = 'PRIMARY'"));
            if( !empty($primaryColumnInfo[0]) ){
                $column = reset($primaryColumnInfo);
                if( $column->Column_name != 'id'){
                    // (new Blueprint($table))->renameColumn($column->Column_name, 'id');
                    Schema::table($table, function(Blueprint $current_table) use ($column){
                        $current_table->renameColumn($column->Column_name, 'id');
                    });
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('id', function (Blueprint $table) {
            //
        });
    }
}
