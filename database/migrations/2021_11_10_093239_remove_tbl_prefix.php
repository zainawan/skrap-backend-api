<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveTblPrefix extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        {
            $tables_meta = Schema::getAllTables();
            $start_strings = ['tbl_'];
            foreach($tables_meta as $index => $table_meta){
                $table = reset($table_meta);
                foreach($start_strings as $start_string){
                    if( preg_match('#^' . $start_string . '#', $table) === 1 ){
                        $new_name = substr($table, strlen($start_string), strlen($table) );
                        Schema::rename($table, $new_name);
                        break;
                    }
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
