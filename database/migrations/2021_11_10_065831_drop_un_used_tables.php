<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use \Illuminate\Support\Facades\DB;

class DropUnUsedTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('tbl_job_req'); // 297 rows
        Schema::dropIfExists('tbl_job_dispute');
        Schema::dropIfExists('tbl_schedule_job'); // Job_scheduled // 373 rows
        Schema::dropIfExists('users');
        Schema::dropIfExists('tbl_cust_cards'); // tbl_cust_card
        Schema::dropIfExists('tbl_given_credits'); // tbl_credit_given // 11 rows
        Schema::dropIfExists('tbl_driver_service_areas');
        Schema::dropIfExists('tbl_driver_services');
        Schema::dropIfExists('tbl_provider_services');
        Schema::dropIfExists('tbl_coupon_service'); // tbl_coupon_services
        Schema::dropIfExists('tbl_coupon_area');
        Schema::dropIfExists('tbl_coupon_merchant'); // tbl_merchant_coupon
        Schema::dropIfExists('tbl_import_coupon');
        Schema::dropIfExists('tbl_provider_wallet');
        Schema::dropIfExists('tbl_provider_bank_acc');
        Schema::dropIfExists('tbl_provider_bank_manual');
        Schema::dropIfExists('tbl_provider_service_areas');
        Schema::dropIfExists('tbl_invitations'); // tbl_invitation
        Schema::dropIfExists('tbl_location_history');
        Schema::dropIfExists('tbl_payment_transaction');
        Schema::dropIfExists('tbl_permit_rates');
        Schema::dropIfExists('tbl_problem_types');
        Schema::dropIfExists('tbl_related_users');
        Schema::dropIfExists('tbl_ride_history');
        Schema::dropIfExists('tbl_skip_extention_cost'); // tbl_extension_cost // 3 rows
        Schema::dropIfExists('tbl_skip_rates'); // 1244 rows
        Schema::dropIfExists('tbl_time_base_notifications'); // tbl_timesbase_notification
        Schema::dropIfExists('tbl_user_benefits'); // tbl_user_benifit // 4 rows
        Schema::dropIfExists('tbl_user_doc_verifications'); // tbl_doc_verification
        Schema::dropIfExists('tbl_user_docs'); // tbl_docs
        Schema::dropIfExists('tbl_user_extra');
        Schema::dropIfExists('tbl_user_role'); // tbl_user_roles
        Schema::dropIfExists('tbl_user_role_tmp'); // tbl_user_role_temp // 239 rows
        Schema::dropIfExists('tbl_vat_detail'); // tbl_vat_details
        Schema::dropIfExists('tbl_wf_actions'); // tbl_wf_action
        Schema::dropIfExists('wf_transactions');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
